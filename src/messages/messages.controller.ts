import { Controller, Get, UseGuards } from '@nestjs/common';
import { CustomQuery } from 'src/decorators/custom-query.decorator';
import { JwtAuthGuard } from 'src/guards/jwt/jwt.guard';
import { MessagesService } from './messages.service';

@Controller('messages')
export class MessagesController {
  constructor(private readonly messagesService: MessagesService) {}

  @UseGuards(JwtAuthGuard)
  @Get('secret-message')
  getSecretMessage(@CustomQuery() messageQry: { userId: string }) {
    return this.messagesService.getSecretMessage(messageQry.userId);
  }
}
