import { Injectable } from '@nestjs/common';
import { MessageRepository } from './repository/message.repository';

@Injectable()
export class MessagesService {
  constructor(private messageRepo: MessageRepository) {}

  getSecretMessage(userId: string) {
    return this.messageRepo.getSecretMessage(userId);
  }
}
