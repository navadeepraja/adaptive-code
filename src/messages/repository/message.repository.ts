import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Message, MessageDocument } from '../entities/messages.entity';

@Injectable()
export class MessageRepository {
  constructor(
    @InjectModel(Message.name) private messageModel: Model<MessageDocument>,
  ) {}

  async getSecretMessage(userId: string) {
    try {
      const response = await this.messageModel.findOne({ userId });
      if (!response) {
        throw new NotFoundException('User Secret Message not found');
      }
      return response;
    } catch (error) {
      throw error;
    }
  }
}
