import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const CustomQuery = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const response = { ...request.query };
    response.userId = request.user?.userId;
    return response;
  },
);
