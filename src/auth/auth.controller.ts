import { Controller, Post, Body, UseGuards, Req } from '@nestjs/common';
import { JwtAuthGuard } from 'src/guards/jwt/jwt.guard';
import { AuthService } from './auth.service';
import { SignUpDto } from './dto/sign-up.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { SignInDto } from './dto/sign-in.dto';
import { Request as ExpRequest } from 'express';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('sign-up')
  signUp(@Body() createAuthDto: SignUpDto) {
    return this.authService.createUser(createAuthDto);
  }

  @UseGuards(JwtAuthGuard)
  @Post('sign-out')
  signOut(@Req() req: ExpRequest) {
    const authToken = req.get('Authorization').replace('Bearer', '').trim();
    return this.authService.signOut(authToken);
  }

  @Post('sign-in')
  signIn(@Body() signInDto: SignInDto) {
    return this.authService.loginUser(signInDto);
  }

  @UseGuards(JwtAuthGuard)
  @Post('reset-password')
  forgotPassword(
    @Body() resetPassword: ResetPasswordDto,
    @Req() req: ExpRequest,
  ) {
    const authToken = req.get('Authorization').replace('Bearer', '').trim();
    return this.authService.resetPassword(authToken, resetPassword);
  }
}
