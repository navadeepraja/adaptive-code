import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  Logger,
  Session,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SessionDocument } from '../entities/sessions.entity';
import { User, UserDocument } from '../entities/users.entity';
import { IUser } from '../interfaces/user.interface';

@Injectable()
export class AuthRepository {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(Session.name) private sessionModel: Model<SessionDocument>,
  ) {}

  signupHandler(user: IUser) {
    try {
      return this.userModel.create(user);
    } catch (error) {
      Logger.error(error);
      throw error;
    }
  }

  async signOutHandler(sessionId: string) {
    try {
      const response = await this.expireSession(sessionId);
      if (response.modifiedCount > 0) {
        return {
          message: 'User signout successful',
        };
      } else {
        throw new HttpException('Session not found', HttpStatus.BAD_REQUEST);
      }
    } catch (error) {
      throw error;
    }
  }

  async signInHandler(emailAddress: string, password: string) {
    try {
      const response = await this.userModel.findOne({ emailAddress, password });
      if (response) {
        const sessionData = await this.createSession(response._id.toString());
        return sessionData;
      } else {
        throw new UnauthorizedException();
      }
    } catch (error) {
      Logger.error(error);
      throw error;
    }
  }

  async forgotPasswordHandler(
    sessionId: string,
    emailAddress: string,
    oldPassword: string,
    newPassword: string,
  ) {
    try {
      const response = await this.userModel.updateOne(
        { emailAddress, password: oldPassword },
        { $set: { password: newPassword } },
      );
      if (response.modifiedCount > 0) {
        const sessionData = await this.expireSession(sessionId);
        if (sessionData.modifiedCount) {
          return {
            message: 'Password Update successful',
          };
        }
      } else {
        throw new BadRequestException('User with details not found');
      }
    } catch (error) {
      Logger.error(error);
      throw error;
    }
  }

  validateSession(sessionId: string) {
    try {
      return this.sessionModel.findOne({ _id: sessionId, isDeleted: false });
    } catch (error) {
      throw error;
    }
  }

  private createSession(userId: string) {
    try {
      return this.sessionModel.create({ userId });
    } catch (error) {
      throw error;
    }
  }

  private expireSession(sessionId: string) {
    try {
      return this.sessionModel.updateOne(
        { _id: sessionId },
        { $set: { isDeleted: true } },
      );
    } catch (error) {
      throw error;
    }
  }
}
