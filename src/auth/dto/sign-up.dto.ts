import { IsEmail, IsOptional, IsString } from 'class-validator';
import { IUser } from '../interfaces/user.interface';

export class SignUpDto implements IUser {
  @IsString()
  firstName: string;

  @IsString()
  @IsOptional()
  lastName: string;

  @IsEmail()
  emailAddress: string;

  @IsString()
  password: string;
}
