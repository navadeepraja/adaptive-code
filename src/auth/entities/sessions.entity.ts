import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type SessionDocument = HydratedDocument<SessionData>;

@Schema()
export class SessionData {
  @Prop({ required: true })
  userId: string;

  @Prop({ required: true, default: false })
  isDeleted: string;
}

export const SessionSchema = SchemaFactory.createForClass(SessionData);
