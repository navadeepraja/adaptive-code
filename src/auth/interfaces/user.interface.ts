export interface IUser {
  firstName: string;
  emailAddress: string;
  password: string;
  lastName?: string;
}
