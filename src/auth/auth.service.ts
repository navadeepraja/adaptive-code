import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import * as md5 from 'md5';
import { SignUpDto } from './dto/sign-up.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { SignInDto } from './dto/sign-in.dto';
import { AuthRepository } from './repository/auth.repository';

@Injectable()
export class AuthService {
  constructor(
    private readonly authRepository: AuthRepository,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  createUser(createAuthDto: SignUpDto) {
    return this.authRepository.signupHandler(createAuthDto);
  }

  async loginUser(signInDto: SignInDto) {
    const { email, password } = signInDto;
    const hash = md5(password);
    try {
      const sessionDetails = await this.authRepository.signInHandler(
        email,
        hash,
      );
      const token = await this.getTokens(sessionDetails._id.toString());
      return {
        token,
      };
    } catch (error) {
      throw error;
    }
  }

  resetPassword(authToken: string, resetPassword: ResetPasswordDto) {
    const sessionId = this.getSessionId(authToken);
    const { email, oldPassword, newPassword } = resetPassword;
    if (oldPassword === newPassword) {
      throw new BadRequestException('Old and New Password cannot be same');
    }
    const oldHash = md5(oldPassword);
    const newHash = md5(newPassword);
    return this.authRepository.forgotPasswordHandler(
      sessionId,
      email,
      oldHash,
      newHash,
    );
  }

  signOut(authToken: string) {
    const sessionId = this.getSessionId(authToken);
    return this.authRepository.signOutHandler(sessionId);
  }

  private async getTokens(sessionId: string) {
    const accessToken = await this.jwtService.signAsync(
      {
        sessionId,
      },
      {
        secret: this.configService.get('JWT_SECRET'),
        expiresIn: '2min',
      },
    );
    return accessToken;
  }

  private getSessionId(token: string) {
    const decoded = this.jwtService.decode(token) as {
      sessionId: string;
    };
    const { sessionId } = decoded;
    return sessionId;
  }
}
